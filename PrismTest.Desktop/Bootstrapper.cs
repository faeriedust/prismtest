﻿using System;
using System.Windows;
using Microsoft.Practices.Prism.MefExtensions;

namespace PrismTest.Desktop {
  public class Bootstrapper : MefBootstrapper {
    /// <remarks>
    /// Allows the region manager service to attach to the shell.
    /// </remarks>
    protected override DependencyObject CreateShell() {
      return new Shell();
    }
    /// <remarks>
    /// Displays the shell to the user
    /// </remarks>
    protected override void InitializeShell() {
      base.InitializeShell();
      Application.Current.MainWindow = (Window) this.Shell;
      Application.Current.MainWindow.Show();
    }
    /// <summary>
    /// Populates the module catalog with modules
    /// </summary>
    protected override void ConfigureModuleCatalog() {
      base.ConfigureModuleCatalog();
    }
  }
}
